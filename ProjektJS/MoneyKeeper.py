from tkinter import *
from tkinter import ttk
from Coin import *

class InvalidObject(Exception):
    '''Invalid Object sends to keeper'''
    pass

class InvalidCoinValue(Exception):
    '''Invalid Coin Value'''
    pass

class NoCoinAvailable(Exception):
    '''There are no coins left with specified value'''
    pass

class MoneyKeeper():
    '''Money Storage: Safe, Input path etc. '''

    Coins = [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000] #Available coins

    def __init__(self):
        '''Constructor'''
        self.__coinsStored = [[] for x in self.Coins] #Prepare slots

    def addCoin(self, coin):
        '''Adds coin to safe'''
        if not isinstance(coin, Coin):
            raise InvalidObject()
        
        if not coin.getValue() in self.Coins:
            raise InvalidCoinValue()

        self.__coinsStored[self.Coins.index(coin.getValue())].append(coin)

    def getCoin(self, value):
        '''Returns coins or throws exception if not available'''
        if len(self.__coinsStored[self.Coins.index(value)]) < 1:
            raise NoCoinAvailable()

        return self.__coinsStored[self.Coins.index(value)].pop()

    def initializeKeeper(self, howMuch):
        '''Sets amount of each coin value'''
        for x in self.__coinsStored:
            x.clear()
            for y in range(howMuch):
                x.append(Coin(self.Coins[self.__coinsStored.index(x)]))
        
    def getFrameWithSummary(self, parent): 
        '''Gets Frame for Debug Window'''

        frm = ttk.Frame(parent);
        frm.grid(column=0, row=0, sticky=(N, W, E, S))
        
        ttk.Label(frm, text='Coin').grid(column = 0, row = 0)
        ttk.Label(frm, text='Count').grid(column = 1, row = 0)

        i = 1
        for x in self.Coins:
            ttk.Label(frm, text=str(x / 100)).grid(column = 0, row = i)
            ttk.Label(frm, text=len(self.__coinsStored[self.Coins.index(x)])).grid(column = 1, row = i)
            i+=1

        return frm

    def getCoinSum(self):
        '''Gets Sum of all coins inside keeper'''
        sum = 0
        for x in self.__coinsStored:
            for y in x:
                sum += y.getValue()

        return sum

    def getCoinAmount(self, coin):
        '''Gets amount of specific coin in keeper'''
        if not isinstance(coin, Coin):
            raise InvalidObject()
        
        if not coin.getValue() in self.Coins:
            raise InvalidCoinValue()

        return len(self.__coinsStored[self.Coins.index(coin.getValue())])




