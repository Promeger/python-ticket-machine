from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from DebugWindow import *
from Machine import *
from SoundManager import * 

import math
import time

class UserInterface:
    '''Class with UI for Ticket Machine'''

    def __init__(self, width, height, machine):
        '''Constructor'''

        #Root window init
        self.__window = Tk()
        self.__w = width
        self.__h = height
               
        #Window Styles
        self.__fStyle=ttk.Style()
        self.__fStyle.configure('MainFrame.TFrame', background='black')
        self.__fStyle.configure('labStyle.TLabel', foreground='white', background='black', font=('TkDefaultFont', 16), anchor='center')
        self.__fStyle.configure('msg.TLabel', foreground='white', background='black', font=('TkDefaultFont', 10), anchor='center')

        #Machine instance
        self.__machine = machine
        self.__machine.setUI(self)
        self.__machine.initDbg()

        #Window Geometry
        x = (self.__window.winfo_screenwidth() // 2) - (width // 2)
        y = (self.__window.winfo_screenheight() // 2) - (height // 2)
        self.__window.geometry('{}x{}+{}+{}'.format(width,height, x, y))
        self.__window.resizable(width=False, height=False)
        self.__window.title('Automat Biletowy MPK')

        #MainFrame
        self.__mainframe=ttk.Frame(self.__window, style='MainFrame.TFrame')
        self.__mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
        self.__mainframe.rowconfigure(0, weight=2)
        self.__mainframe.rowconfigure(1, weight=6, minsize=640)

        #Logo Bar
        mpkLogo = PhotoImage(file='assets/logo.png', master = self.__window)
        self.__logoLabel = ttk.Label(self.__mainframe, image=mpkLogo).grid(row=0,column=0)
        
        #Input Coins Window
        self.__moneyWindow = Tk()
        self.__moneyWindow.title('Wrzutnik')
        self.__moneyWindowFrame = Frame(self.__moneyWindow)
        self.__moneyWindowFrame.pack(side=TOP, fill=BOTH, expand=1, anchor=N)
        self.preventInputDisabled() #Hide Window

        #Coins count entry
        ttk.Label(self.__moneyWindowFrame, text='Liczba monet do wrzucenia').grid(column=0,row=0)
        self.__monQtyEntr = ttk.Entry(self.__moneyWindowFrame)
        self.__monQtyEntr.insert(0, '1')
        self.__monQtyEntr.grid(column = 0, row = 1)
        self.__monBtnFrame = Frame(self.__moneyWindowFrame)
        self.__monBtnFrame.grid(column=0,row=2)

        #Insert coin(s) after button press
        def insertCoinWnd(coin):
            try:
                value=int(self.__monQtyEntr.get())
            except ValueError:
                messagebox.showerror('Błąd!', 'Wpisz liczbę całkowitą!!')
            else:
                if(value < 0):
                    messagebox.showerror('Błąd!', 'Wpisz liczbę dodatnią!!')
                else:
                    n = value - 1
                    for i in range(value):
                        self.__machine.insertCoin(coin, n)
                        n -= 1
                        
          

        #Coins buttons
        i = 0
        for x in MoneyKeeper.Coins:
            Button(self.__monBtnFrame, text=str(x / 100), command=lambda c=x: insertCoinWnd(Coin(c))).grid(column = i, row = 0)
            i+=1
        
        #MainScreen
        self.renderTicketsFrame()
        
        self.__window.mainloop()

    def preventInputDisabled(self):
        '''Hides Input Coins Window'''
        try:
            self.__moneyWindow.withdraw()
        except AttributeError:
            print('Internal Error')

    def renderTicketsFrame(self):
        '''Renders Tickets Frame'''
        #Hide Input Coins Window
        self.preventInputDisabled()

        #Sound
        SoundManager.confirmationSound()

        #Ticket Buttons Frame
        self.__contentFrame = ttk.Frame(self.__mainframe, style='MainFrame.TFrame')
        self.__contentFrame.grid(column=0, row=1, sticky=(N, S, E, W))

        #Header
        ttk.Label(self.__contentFrame, text='Dotknij aby kupić',style='labStyle.TLabel').grid(row=0, column=1)
      
        #Tickets
        rowList = math.ceil(len(Machine.getAvailableTickets()) / 2)
        availableTickets = Machine.getAvailableTickets()
    
        #Tickets buttons
        for x in range(rowList):
            availableTickets[2*x].__img = PhotoImage(file=availableTickets[2*x].getImage(), master = self.__window)
            ttk.Button(self.__contentFrame, image = availableTickets[2*x].__img, style='labStyle.TLabel', command=lambda t=availableTickets[2*x]: self.renderQtyPrompt(t)).grid(row=x+1, column=0, sticky=(W), ipady=5)

            availableTickets[2*x + 1].__img = PhotoImage(file=availableTickets[2*x + 1].getImage(), master = self.__window)
            ttk.Button(self.__contentFrame, image = availableTickets[2*x + 1].__img, style='labStyle.TLabel', command=lambda t=availableTickets[2*x + 1]: self.renderQtyPrompt(t)).grid(row=x+1, column=2, sticky=(E), ipady=5)


    def renderQtyPrompt(self, ticket):
        '''Renders Quantity Prompt'''

        #Hide Input Coins Window
        self.preventInputDisabled()

        #Sound
        SoundManager.confirmationSound()
        

        #Prompt Frame
        self.__contentFrame = ttk.Frame(self.__mainframe, style='MainFrame.TFrame')
        self.__contentFrame.grid(column=0, row=1, sticky=(N, S, E, W))
        self.__contentFrame.columnconfigure(0, weight=1)

        #Header
        ttk.Label(self.__contentFrame, text='Podaj ilość biletu '+ticket.getName()+' jaką chcesz zakupić',style='labStyle.TLabel').grid(row=0, column=0, sticky=(E, W))

        #Quantity Entry
        self.__qtyEntry = Entry(self.__contentFrame, disabledbackground='#03A9F4', disabledforeground='white', font=('TkDefaultFont', 25), state=DISABLED)
        self.__qtyEntry.grid(row=1, column=0, sticky=(E, W), padx=50, pady=30)
        
        #Digits function
        def appendToQty(digit):
            if digit != 'c':
                SoundManager.confirmationSound()
               
                self.__qtyEntry.configure(state=NORMAL)
                self.__qtyEntry.insert(END, str(digit))
                self.__qtyEntry.configure(state=DISABLED)
            else:
                SoundManager.abortSound()
                self.__qtyEntry.configure(state=NORMAL)
                self.__qtyEntry.delete(len(self.__qtyEntry.get()) - 1, END)
                self.__qtyEntry.configure(state=DISABLED)


        #Buttons
        self.__btns = ttk.Frame(self.__contentFrame, style='MainFrame.TFrame')
        self.__btns.grid(row=2, column=0, ipadx=50, ipady=50);
        self.__btns.columnconfigure(0, weight=1)
        self.__btns.columnconfigure(1, weight=1)
        self.__btns.columnconfigure(2, weight=1)
        self.__btns.grid(row=2, column=0, sticky=(N, E, S, W))
        for x in range(1, 10):
            Button(self.__btns, text=str(x), background='#03A9F4', foreground='white', width= 5, font=('TkDefaultFont', 25), command=lambda a=x: (appendToQty(a))).grid(row=(x-1) // 3, column = (x-1) % 3, pady = 10)
        
        Button(self.__btns, text=str(0), background='#03A9F4', foreground='white', width= 5, font=('TkDefaultFont', 25), command=lambda: (appendToQty(0))).grid(row=3, column = 1, pady = 10)
        
        #Confirm quantity
        def confirmTickets():
            SoundManager.confirmationSound()
            value = 0
            try:
                value=int(self.__qtyEntry.get())
            except ValueError:
                messagebox.showwarning('Uwaga!', 'Niepoprawna wartość! Automat przyjmie ją jako "0"')
                #If invalid value: Add zero
            
            self.__machine.requestTicket(ticket, value)
            self.renderSummaryFrame()

        Button(self.__btns, text='OK', background='#03A9F4', foreground='white', width= 5, font=('TkDefaultFont', 25), command=lambda: confirmTickets()).grid(row=4, column=0)
        Button(self.__btns, text='C', background='#03A9F4', foreground='white', width= 5, font=('TkDefaultFont', 25), command=lambda: (appendToQty('c'))).grid(row=4, column=2)

    def renderSummaryFrame(self):
        '''Frame with summary'''

        #Frame
        self.__contentFrame = ttk.Frame(self.__mainframe, style='MainFrame.TFrame')
        self.__contentFrame.grid(column=0, row=1, sticky=(N, S, E, W))
        self.__contentFrame.columnconfigure(0, weight=1)

        #Header
        ttk.Label(self.__contentFrame, text='Podsumowanie biletów',style='labStyle.TLabel').grid(row=0, column=0, sticky=(E, W))

        #Grid with tickets
        self.__zamBil = ttk.Frame(self.__contentFrame, style='MainFrame.TFrame')
        self.__zamBil.grid(row=1, column=0, sticky=(N, E, S, W), ipadx=50, padx=50);

        i = 0
        for x in self.__machine.AvailableTickets:
            ttk.Label(self.__zamBil, text=x.getName() + 4 * '\t',style='labStyle.TLabel').grid(row=i, column=0, sticky=(W))
            ttk.Label(self.__zamBil, text=self.__machine.getRequestedTickets(x),style='labStyle.TLabel').grid(row=i, column=1, sticky=(E))
            i+= 1

        ttk.Label(self.__contentFrame, text='Suma: '+ '{0:.2f}'.format(self.__machine.getRequestedTicketsPrice() / 100) + 'PLN',style='labStyle.TLabel').grid(row=2, column=0, sticky=(E, W))
        ttk.Label(self.__contentFrame, text='Do zapaty: '+ '{0:.2f}'.format((self.__machine.getRequestedTicketsPrice() - self.__machine.getCoinInInput()) / 100) + 'PLN',style='labStyle.TLabel').grid(row=3, column=0, sticky=(E, W))
        ttk.Label(self.__contentFrame, text='Akceptowalne nominały: 1gr, 2gr, 5gr, 10gr, 20gr, 50gr, 1zł, 2zł, 5zł, 10zł, 20zł, 50zł',style='msg.TLabel').grid(row=4, column=0, sticky=(E, W))
        Button(self.__contentFrame, text='DODAJ BILET', background='#03A9F4', foreground='white',  font=('TkDefaultFont', 25), command=lambda: self.renderTicketsFrame()).grid(row=5, column=0, sticky=(E, W), pady=20, padx=50)
        Button(self.__contentFrame, text='ANULUJ ZAKUP', background='#03A9F4', foreground='white',  font=('TkDefaultFont', 25), command=lambda: self.cancelPurchase()).grid(row=6, column=0, sticky=(E, W), pady = 20, padx=50)

        #Show Input Coins Window
        self.__moneyWindow.deiconify()
   
    def renderPrintDialog(self):
        '''Printing Dialog'''

        #Sound
        SoundManager.confirmationSound()

        #Close Input Coins Window
        self.preventInputDisabled()

        #Frame
        self.__contentFrame = ttk.Frame(self.__mainframe, style='MainFrame.TFrame')
        self.__contentFrame.grid(column=0, row=1, sticky=(N, S, E, W))
        self.__contentFrame.columnconfigure(0, weight=1)
        ttk.Label(self.__contentFrame, text='Bilety zostały wydrukowane!',style='labStyle.TLabel').grid(row=0, column=0, sticky=(E, W))
        Button(self.__contentFrame, text='OK', background='#03A9F4', foreground='white',  font=('TkDefaultFont', 25), command=lambda: self.renderTicketsFrame()).grid(row=1, column=0, sticky=(E, W), pady = 20, padx=50)

    def renderMessage(self, str):
        '''Renders Message'''

        #Close Input Coins Window
        self.preventInputDisabled()

        #Frame
        self.__contentFrame = ttk.Frame(self.__mainframe, style='MainFrame.TFrame')
        self.__contentFrame.grid(column=0, row=1, sticky=(N, S, E, W))
        self.__contentFrame.columnconfigure(0, weight=1)
        ttk.Label(self.__contentFrame, text=str, style='labStyle.TLabel').grid(row=0, column=0, sticky=(E, W))


        Button(self.__contentFrame, text='OK', background='#03A9F4', foreground='white',  font=('TkDefaultFont', 25), command=lambda: self.cancelPurchase()).grid(row=1, column=0, sticky=(E, W), pady = 20, padx=50)


    def cancelPurchase(self):
        '''Cancels purchase'''

        self.preventInputDisabled()
        SoundManager.abortSound()
        self.__machine.cancelAll()
        self.renderTicketsFrame()

