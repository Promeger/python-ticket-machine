from Valueable import *

class Coin(Valueable):
    '''Simple coin'''

    def __init__(self, value):
        '''Constructor'''
        super().__init__(value)

    def getCurrency(self):
        '''Currently we can only support PLN'''
        return "PLN"