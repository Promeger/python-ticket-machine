from Ticket import *
from MoneyKeeper import *
import DebugWindow
import time

class Machine():
    """Backend for Ticket Machine"""

    #Available Tickets
    AvailableTickets = [Ticket("Normalny 20min", False, 280, "assets/norm20.png"),
                        Ticket("Normalny 40min", False, 380, "assets/norm40.png"),
                        Ticket("Normalny 60min", False, 500, "assets/norm60.png"),
                        Ticket("Ulgowy 20min", True, 140, "assets/ul20.png"), 
                        Ticket("Ulgowy 40min", True, 190, "assets/ul40.png"), 
                        Ticket("Ulgowy 60min", True, 250, "assets/ul60.png")]

    def __init__(self):
        '''Constructor'''

        #Initialize safes
        self.__inputMoneyKeeper = MoneyKeeper()
        self.__safe = MoneyKeeper()
        
        #Preserve no tickets already ordered
        self.cancelAll()

    @staticmethod
    def getAvailableTickets():
        '''Gets Available Tickets'''
        return Machine.AvailableTickets

    def initializeSafe(self, count):
        '''Initializes internal safe'''
        self.__safe.initializeKeeper(count)

    def setUI(self, ui):
        '''Sets UI (frontend)'''
        self.__ui = ui
        
    def insertCoin(self, coin, i):
        '''Insert coin into the machine'''

        try:
            self.__inputMoneyKeeper.addCoin(coin)
        except (InvalidObject, InvalidCoinValue):
            print('Inserted and dropped unknown object')
        else:
            print('Inserted coin:', coin.getValue())

        if i == 0:
            self.updateDebugWindow()

        if self.__inputMoneyKeeper.getCoinSum() >= self.getRequestedTicketsPrice():
            self.purchase()
        else:
            if i == 0:
                self.__ui.renderSummaryFrame()

    def getCoinInInput(self):
        '''Gets Sum of Coins in Input Keeper'''
        return self.__inputMoneyKeeper.getCoinSum()

    def requestTicket(self, ticket, quantity):
        '''Requests New Ticket'''
        self.__requestedTickets[Machine.AvailableTickets.index(ticket)] += quantity

    def getRequestedTickets(self, ticket):
        '''Gets count of requesteds tickets'''
        return self.__requestedTickets[Machine.AvailableTickets.index(ticket)]

    def getRequestedTicketsPrice(self):
        '''Gets Price of all requested tickets'''
        sum = 0
        for x in Machine.AvailableTickets:
            sum += self.__requestedTickets[Machine.AvailableTickets.index(x)]  * x.getValue()

        return sum

    def purchase(self):
        '''Purchases Tickets'''
        rest = self.canProcessPayment()

        if rest == -1:
           self.__ui.renderMessage('Płatność tylko odliczoną kwotą!')
        elif rest == 0:
            self.__ui.renderPrintDialog()

            for x in MoneyKeeper.Coins:
                while True:
                    try:
                        c = self.__inputMoneyKeeper.getCoin(x)
                        self.__safe.addCoin(c)
                    except NoCoinAvailable:
                        break

            self.processTickets()
            self.updateDebugWindow()
        else:
            print('Calculated rest: ',rest)
            self.__ui.renderPrintDialog()

            
            for x in MoneyKeeper.Coins:
                while True:
                    try:
                        c = self.__inputMoneyKeeper.getCoin(x)
                        self.__safe.addCoin(c)
                    except NoCoinAvailable:
                        break


            #Give rest
            i = len(MoneyKeeper.Coins) - 1
            while rest > 0 and i >= 0:
                if rest > MoneyKeeper.Coins[i]:
                    hmc = rest // MoneyKeeper.Coins[i]
                    rest -= hmc * MoneyKeeper.Coins[i]

                    for x in range(hmc):
                        c = self.__safe.getCoin(MoneyKeeper.Coins[i])
                        print('Dropped coin:' + str(c.getValue()))

                i -= 1

            self.processTickets()
            self.updateDebugWindow()


    def cancelAll(self):
        '''Cancels purchase'''
        self.__requestedTickets = [0 for x in Machine.AvailableTickets]

        for x in MoneyKeeper.Coins:
            while True:
                try:
                    c = self.__inputMoneyKeeper.getCoin(x)
                    print('Dropped coin:' + str(c.getValue()))
                except NoCoinAvailable:
                    break
    
    def processTickets(self):
        for x in self.printTicket():
            print('Printing:' + x.getName())
            time.sleep(0.1)

        self.cancelAll()

    def printTicket(self):
        '''Prints tickets'''
        for x in self.__requestedTickets:
            for y in range(x):
                yield Machine.AvailableTickets[self.__requestedTickets.index(x)]
    




    def canProcessPayment(self):
        '''Calculates if machine can give rest'''
        rest = self.getCoinInInput() - self.getRequestedTicketsPrice()

        if rest == 0:
            return 0 #There is no need to calculate rest
        else:
            #Get all available coins
            availCoins = [0 for x in MoneyKeeper.Coins];
            for x in MoneyKeeper.Coins:
                availCoins[MoneyKeeper.Coins.index(x)] = self.__inputMoneyKeeper.getCoinAmount(Coin(x)) + self.__safe.getCoinAmount(Coin(x))

            #Calculate if I can give rest
            neededCoins = [ 0 for x in MoneyKeeper.Coins]

            i = len(availCoins) - 1
            rToCalc = rest
            while rToCalc > 0 and i >= 0:
                if rToCalc > MoneyKeeper.Coins[i]:
                    hmc = rToCalc // MoneyKeeper.Coins[i]
                    rToCalc -= hmc * MoneyKeeper.Coins[i]

                    neededCoins[MoneyKeeper.Coins.index(x)] += hmc

                i -= 1

            for x in range(len(MoneyKeeper.Coins)):
                if availCoins[x] < neededCoins[x]:
                    return -1

            return rest

    def updateDebugWindow(self):
        '''Updates Debug Window'''
        Label(self.__debugWindow.getFrame(), text='Input Keeper').grid(column = 0, row = 0)
        self.__inpFrame = self.__inputMoneyKeeper.getFrameWithSummary(self.__debugWindow.getFrame())
        self.__inpFrame.grid(column=0, row=1)
        Label(self.__debugWindow.getFrame(), text='Safe').grid(column = 2, row = 0)
        self.__safFrame = self.__safe.getFrameWithSummary(self.__debugWindow.getFrame())
        self.__safFrame.grid(column=2, row=1)
        Label(self.__debugWindow.getFrame(), text='\t\t\t\t').grid(column = 1, row = 0)

    def initDbg(self):
        '''Initializes Debugger Window'''
        self.__debugWindow = DebugWindow.DebugWindow()
        self.updateDebugWindow()