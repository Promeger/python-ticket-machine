import platform
import _thread

if platform.system() == 'Windows':
   import winsound


class SoundManager:
    '''Simple Sound Effects: WINDOWS Only (Getting OS)'''

    @staticmethod
    def confirmationSound():
        '''Plays confirmation sound'''
        if platform.system() != 'Windows': #Check OS
            return

        _thread.start_new_thread ( playSnd, ('Assets\\confirmation.wav', )) #Start in new thread
    
    @staticmethod
    def abortSound():
        '''Plays abort sound'''
        if platform.system() != 'Windows': #Check OS
            return

        _thread.start_new_thread ( playSnd, ('Assets\\abort.wav', )) #Start in new thread



def playSnd(path):
    '''Execute play sound'''
    winsound.PlaySound(path, winsound.SND_FILENAME) #Play