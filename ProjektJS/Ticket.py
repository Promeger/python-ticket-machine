from Valueable import *

class Ticket(Valueable):
    '''Represents Ticket'''

    def __init__(self, name, hasDsicount, price, image):
        '''Constructor'''
        self.__name = name
        self.__discounted = hasDsicount
        self.__image = image
        self.setValue(price)

    def getName(self):
        '''Gets name of the ticket'''
        return self.__name

    def setName(self, name):
        '''Sets name of the ticket'''
        self.__name = name

    def setImage(self, image):
        '''Sets image (path)'''
        self.__image = image

    def getImage(self):
        '''Gets image (path)'''
        return self.__image

    def getValue(self):
        '''Gets Value (Base value stored in valueable is constant for normal / reduced tickets)'''
        if self.__discounted == False:
            return super().getValue()
        else:
            return super().getValue() // 2

    def setValue(self, value):
        '''Sets Value (Base value stored in valueable is constant for normal / reduced tickets)'''
        if self.__discounted:
            super().setValue(value * 2)
        else:
            super().setValue(value)
        


