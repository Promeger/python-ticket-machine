class Valueable():
    '''Represents object with value'''
    def __init__(self, val):
        '''Constructor'''
        self.__value = val

    def setValue(self, val):
        '''Sets Value'''
        self.__value = val

    def getValue(self):
        '''Gets Value'''
        return self.__value;


