#Python Ticket Machine
Simple Python Window Application made for Python Classes (Symbolic Languages on Cracow University of Technology). It allows you to buy 3 types of tickets (each with or without discount) and pay with Polish Zloty coins or banknotes).
The Application was designed to be a modular implementation of task. You can simulate multiple machines inside one Python proess.

##Requirements
+ Python 3.6 or higher
+ Tinkter
+ Windound (If you use Windows, otherwise application won't play any sounds)

##How to use
Simply launch ProjektJS.py and follow on screen hints (In Polish).
There is a Main Window - User Interface, simple service Window that allows you to see what types of coins are inside machine. When You are able to insert coins. There will be third window - Input

##Copyright
Copyright (C) 2018 by Promeger